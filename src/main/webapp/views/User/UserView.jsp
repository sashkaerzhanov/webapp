<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  User: Alex Erzhanov
  Date: 19.09.2017
  Time: 19:55
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>USERS</title>
</head>
<body>
<a href="${pageContext.servletContext.contextPath}/views/User/AddUser.jsp">Add USER</a>
<table style='border : 1px solid rgba(238,92,49,0.69)'>
    <tr>
        <td>LOGIN</td>
        <td>EMAIL</td>
        <td>ACTIONS</td>
    </tr>
    <c:forEach items="${users}" var="user" varStatus="status">
            <tr>
                <td> ${user.login}</td>
                <td> ${user.email}</td>
                <td>
                    <a href="${page.Context.servletContext.contextPath}/user/edit?id=${user.id}">EDIT</a>
                    <a href="${page.Context.servletContext.contextPath}/user/delete?id=${user.id}">DELETE</a>
                </td>
            </tr>
</table>
    </c:forEach>
</body>
</html>
