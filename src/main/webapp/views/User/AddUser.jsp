<%--
  User: Alex Erzhanov
  Date: 19.09.2017
  Time: 22:09
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add User</title>
</head>
<body>
    <form action="${pageContext.servletContext.contextPath}/user/create" method="post">
        <table>
            <tr>
                <td>Login</td>
                <td>
                    <input type="text" name="login">
                </td>
            </tr>
            <tr>
                <td>E-mail</td>
                <td>
                    <input type="text" name="email">
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <input type="text" name="password">
                </td>
            </tr>
            <tr>
                <td><input type="submit" align="center" value="Submit"/></td>
            </tr>
        </table>
    </form>
</body>
</html>
