package system.models;


/**
 * @author Alexander Erzhanov
 * @since 18.07.2017
 */
public class Car implements Vehicle {


    private final String carModel;
    private final String clientName;


    public Car(String carModel, String clientName) {

        this.carModel = carModel;
        this.clientName = clientName;
    }



    /**
     * Возвращает модель транспортного средства
     */

    public String getVehicleModel() {
        return carModel;
    }

    public String getClient() {
        return clientName;
    }


    /**
     * @return hashCode
     */

//    public int hashCode() {
//        int result = carModel != null ? carModel.hashCode() : 0;
//        result = 31 * result + carYear;
//        return result;
//    }
//
//
//
//    @Override
//    public boolean equals(Object o) {
//        if (o == null || getClass() != o.getClass()) {
//            return false;
//        }
//        Car that = (Car) o;
//
//        if (this.carYear != that.carYear) {
//            return false;
//        }
//        if (this.carMake == null) {
//            return that.carMake == null;
//        } else {
//            return this.carMake.equals(that.carMake);
//        }
//    }
//
//
//    @Override
//    public String toString() {
//        return "Car{" +
//                "carMake='" + carMake + '\'' +
//                ", carModel='" + carModel + '\'' +
//                ", carYear=" + carYear +
//                '}';
//    }


}