package system.dao;

import system.models.User;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * singleton
 * @author Alexander Erzhanov
 * @since 19.09.2017
 */
public class UserCasche {
    private static final UserCasche INSTANCE = new UserCasche();

    private final ConcurrentHashMap<Integer, User> users = new ConcurrentHashMap<Integer, User>();



    public static UserCasche getInstance(){
        return INSTANCE;
    }

    public Collection<User> values(){
        return this.users.values();
    }

    public void add(final User user){
        this.users.put(user.getId(),user);
    }

    public void edit(final User user){
        this.users.replace(user.getId(),user);
    }

    public void delete(final User user){
        this.users.remove(user.getId(),user);
    }

    public void get(final int id){
        this.users.get(id);
    }
}
