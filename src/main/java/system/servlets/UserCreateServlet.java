package system.servlets;

import system.dao.UserCasche;
import system.models.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Alexander Erzhanov
 * @since 19.09.2017
 */
public class UserCreateServlet extends HttpServlet {
    private final UserCasche USER_CASCHE = UserCasche.getInstance();
    final AtomicInteger ID = new AtomicInteger();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.USER_CASCHE.add(new User(this.ID.incrementAndGet(),req.getParameter("login"),req.getParameter("email"),req.getParameter("password")));
        resp.sendRedirect(String.format("%s%s",req.getContextPath(),"/user/view"));
    }
}
