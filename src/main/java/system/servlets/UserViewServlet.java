package system.servlets;

import system.dao.UserCasche;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @author Alexander Erzhanov
 * @since 19.09.2017
 */
public class UserViewServlet extends HttpServlet {
    private final UserCasche USER_CASCHE = UserCasche.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("users", this.USER_CASCHE.values());
        RequestDispatcher dispatcher = req.getRequestDispatcher("/views/User/UserView.jsp");
        dispatcher.forward(req, resp);
    }
}