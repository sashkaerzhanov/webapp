package system.servlets;

import system.models.Car;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author Alexander Erzhanov
 * @since 05.09.2017
 */
public class CarsServlet extends HttpServlet {

    final List<Car> cars = new CopyOnWriteArrayList<Car>();
    private String findName;

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();
        writer.append(
                "<!DOCTYPE html>" +
                        "<html>" +
                        "<head>" +
                        "     <title>Cars</title>" +
                        "</head>" +
                        "<body>" +
                        "     <form action='" + req.getContextPath() + "/' method='post'>" +
                        "           Car : <input type='text' name='model'>" +
                        "         Owner : <input type='text' name='client'>" +
                        "         <input type='submit' value='Submit'>" +
                        "     <form>" +
                        "<form action='" + req.getContextPath() + "/' method='post'>" +
                        "       Search : <input type='text' name='search'>" +
                        "       <input type='submit' value='Search'>" +
                        "<form>" +
                        this.viewCars() +
                        this.findPets(findName) +
                        "</body>" +
                        "</html>"
        );
        writer.flush();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!req.getParameter("model").isEmpty() && !req.getParameter("client").isEmpty()) {
            this.cars.add(new Car(req.getParameter("model"),req.getParameter("client") ));
        }

        if (!req.getParameter("search").isEmpty()) {
            this.findName = req.getParameter("search");
        }
        doGet(req, resp);
    }





    private String viewCars() {
        StringBuilder sb = new StringBuilder();

        sb.append("<p> Cars Info </p>");
        sb.append("<table style='border : 1px solid black'>");

        sb.append("<tr><td style='border : 1px solid black'>").append("  CAR MODEL  ").append("</td>"+
                "<td style='border : 1px solid black'>").append(" CLIENT NAME ").append("</td></tr>");


        for (Car car : this.cars) {
            sb.append("<tr><td style='border : 1px solid black'>").append(car.getVehicleModel()).append("</td>" +
                    "<td style='border : 1px solid black'>").append(car.getClient()).append("</td></tr>");
        }

        sb.append("</table>");
        return sb.toString();
    }


    private String findPets(String carModel) {
        StringBuilder sb = new StringBuilder();
        boolean finded = false;
        if (!cars.isEmpty() && carModel != null) {
            sb.append("<p>Cars found:</p>");
            sb.append("<table>");
            sb.append("<tr><td>").append(" CLIENT NAME: </td><td> CAR MODEL: </td><td> PET AGE: </td></tr>");
            for (Car car : this.cars) {
                if (carModel.equals(car.getVehicleModel())) {
                    sb.append("<tr><td>").append(car.getClient()).append("</td><td>").append(car.getVehicleModel()).append("</td></tr>");
                    finded = true;
                }
            }
            sb.append("</table>");
        }
        if (!finded) {
            sb.delete(0, sb.capacity());
            sb.append("<p>Not Found</p>");
        }
        return sb.toString();
    }
}
